package main

import (
	"text/template"

	"bitbucket.org/luisGsandoval/lu_sa-backend/handlers"
	"bitbucket.org/luisGsandoval/lu_sa-backend/routers"
)

// Tpl is the templates initiater to user templates
var Tpl *template.Template

func init() {
	routers.Tpl = template.Must(template.ParseGlob("templates/*.html"))
}

func main() {

	handlers.HandleFuncs()
}
