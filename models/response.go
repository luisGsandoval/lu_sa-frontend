package models

// ClientResponse is a struct that is going to be used to marshall the data to send back to the client
type ClientResponse struct {
	Status  int    `json:"status"`
	Message string `json:"message"`
	Data    string `json:"data"`
}
