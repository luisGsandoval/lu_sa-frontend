package models

// User es el struct que me permite definir el modelo para usuarios
type User struct {
	ID       int    `json:"id"`
	Email    string `json:"email"`
	Password string `json:"password,omitempty"`

	Name      string `json:"name,omitempty"`
	Lastname  string `json:"lastname,omitempty"`
	Telephone string `json:"telephone"`
	Address   string `json:"address"`

	ExpiresAt float64 `json:"exp,omitempty"`
	Token     string  `json:"token,omitempty"`
}
