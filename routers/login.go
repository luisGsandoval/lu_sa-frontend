package routers

import (
	"encoding/json"
	"io/ioutil"
	"log"
	"net/http"
	"strings"

	"bitbucket.org/luisGsandoval/lu_sa-backend/config"
	"bitbucket.org/luisGsandoval/lu_sa-backend/models"
	"github.com/dgrijalva/jwt-go"
)

// Login is the handler that sends the login html
func Login(w http.ResponseWriter, r *http.Request) {

	err := Tpl.ExecuteTemplate(w, "loginForm.html", nil)
	if err != nil {
		log.Panic(err)
	}
}

// LoginPost is the handler that retrieves the login form info
func LoginPost(w http.ResponseWriter, r *http.Request) {

	if r.Method != "POST" {
		http.Redirect(w, r, "/login", http.StatusSeeOther)
		return
	}

	var t models.User
	var cliRes models.ClientResponse

	t.Email = r.FormValue("email")
	t.Password = r.FormValue("password")

	e, err := json.Marshal(t)

	if err != nil {
		log.Panic(err)
		return
	}

	res, err := http.Post(config.Get("BACKEND")+"/login", "application/json; charset=UTF-8", strings.NewReader(string(e)))
	if err != nil {
		log.Panic(err)
		cliRes.Message = "Server error: " + err.Error()
		cliRes.Status = res.StatusCode
		err = Tpl.ExecuteTemplate(w, "loginForm.html", cliRes)
		return
	}
	// close response body
	defer res.Body.Close()
	// read response data
	data, _ := ioutil.ReadAll(res.Body)

	if res.StatusCode != 201 {

		cliRes.Message = "Error: " + string(data)
		cliRes.Status = res.StatusCode
		err = Tpl.ExecuteTemplate(w, "loginForm.html", cliRes)
		return
	}

	var lognRsp models.LoginResponse

	err = json.Unmarshal([]byte(data), &lognRsp)
	if err != nil {
		cliRes.Message = "Error: " + string(data)
		cliRes.Status = res.StatusCode
		err = Tpl.ExecuteTemplate(w, "loginForm.html", cliRes)
		log.Panic(err)
		return
	}

	claims := jwt.MapClaims{}
	_, err = jwt.ParseWithClaims(lognRsp.Token, claims, func(token *jwt.Token) (interface{}, error) {
		return []byte(config.Get("JWTSECRET")), nil
	})

	for key, val := range claims {

		switch key {
		case "name":
			t.Name = val.(string)
		case "email":
			t.Email = val.(string)
		case "lastname":
			t.Lastname = val.(string)
		case "telephone":
			t.Telephone = val.(string)
		case "address":
			t.Address = val.(string)
		case "exp":
			t.ExpiresAt = val.(float64)
		}
	}

	t.Token = string(data)

	err = Tpl.ExecuteTemplate(w, "profile.html", t)

	if err != nil {
		log.Fatal(err)
	}

}
