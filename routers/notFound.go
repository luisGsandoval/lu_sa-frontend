package routers

import (
	"net/http"
)

// NotFound is the handler that sends the notFound html
func NotFound(w http.ResponseWriter, r *http.Request) {
	Tpl.ExecuteTemplate(w, "notFound.html", nil)
}
