package routers

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"log"
	"net/http"
	"strings"

	"bitbucket.org/luisGsandoval/lu_sa-backend/config"
	"bitbucket.org/luisGsandoval/lu_sa-backend/models"
)

// Singup is the handler that sends the login html
func Singup(w http.ResponseWriter, r *http.Request) {
	err := Tpl.ExecuteTemplate(w, "signupForm.html", nil)
	if err != nil {
		log.Panic(err)
	}
}

// SignupPost is the handler that retrieves the signup form info
func SignupPost(w http.ResponseWriter, r *http.Request) {

	if r.Method != "POST" {
		http.Redirect(w, r, "/signup", http.StatusSeeOther)
		return
	}

	var t models.User
	var cliRes models.ClientResponse

	t.Email = r.FormValue("email")
	t.Password = r.FormValue("password")

	e, err := json.Marshal(t)

	if err != nil {
		log.Panic(err)
	}

	res, err := http.Post(config.Get("BACKEND")+"/registration", "application/json; charset=UTF-8", strings.NewReader(string(e)))
	if err != nil {
		// log.Panic(err)
		cliRes.Message = "Error:" + err.Error()
		cliRes.Status = res.StatusCode
		err = Tpl.ExecuteTemplate(w, "signupForm.html", cliRes)

		if err != nil {
			log.Panic(err)
		}
		return
	}
	// close response body
	defer res.Body.Close()
	// read response data
	data, _ := ioutil.ReadAll(res.Body)

	if res.StatusCode != 201 && res.StatusCode != 200 {

		cliRes.Message = "Error: " + string(data)
		cliRes.Status = res.StatusCode
		err = Tpl.ExecuteTemplate(w, "signupForm.html", cliRes)
		if err != nil {
			log.Panic(err)
		}
		return
	}

	cliRes.Message = "Succesfully created " + string(data)
	cliRes.Status = res.StatusCode

	fmt.Println(cliRes)

	err = Tpl.ExecuteTemplate(w, "loginForm.html", cliRes)
	if err != nil {
		log.Panic(err)
	}
	return
}
