package routers

import (
	"encoding/json"
	"io/ioutil"
	"log"
	"net/http"
	"strings"

	"bitbucket.org/luisGsandoval/lu_sa-backend/config"
	"bitbucket.org/luisGsandoval/lu_sa-backend/models"
)

// Profile is the handler that sends the profile html
func Profile(w http.ResponseWriter, r *http.Request) {
	Tpl.ExecuteTemplate(w, "profile.html", nil)
}

// EditprofileGet is the handler that sends the editProfile html
func EditprofileGet(w http.ResponseWriter, r *http.Request) {
	Tpl.ExecuteTemplate(w, "editProfile.html", nil)
}

// EditprofilePost is the handler that retrieves the editProfile form info
func EditprofilePost(w http.ResponseWriter, r *http.Request) {

	var t models.User
	var cliRes models.ClientResponse

	if r.Method != "POST" {
		http.Redirect(w, r, "/not-found", http.StatusSeeOther)
		return
	}

	t.Name = r.FormValue("name")
	t.Lastname = r.FormValue("lastname")
	t.Email = r.FormValue("email")
	t.Telephone = r.FormValue("telephone")
	t.Address = r.FormValue("address")
	// t.Token = r.FormValue("jwt")

	e, err := json.Marshal(t)

	// res, err := http.Post(config.Get("BACKEND")+"/edit-profile", "application/json; charset=UTF-8", strings.NewReader(string(e)))

	url := config.Get("BACKEND") + "/edit-profile"

	// Create a Bearer string by appending string access token
	var jwt = r.FormValue("jwt")

	// Create a new request using http
	req, err := http.NewRequest("POST", url, strings.NewReader(string(e)))

	// add authorization header to the req
	req.Header.Add("Authorization", jwt)

	// Send req using http Client
	client := &http.Client{}

	resp, err := client.Do(req)
	if err != nil {
		log.Println("Error: ", err)
		return
	}

	if err != nil {
		log.Panic(err)
		cliRes.Message = "Server error: " + err.Error()
		cliRes.Status = resp.StatusCode
		err = Tpl.ExecuteTemplate(w, "editProfile.html", cliRes)
		return
	}

	// close response body
	defer resp.Body.Close()

	// read response data
	data, err := ioutil.ReadAll(resp.Body)

	if err != nil {
		log.Panic(err)
		cliRes.Message = "Server error: " + err.Error()
		cliRes.Status = resp.StatusCode
		err = Tpl.ExecuteTemplate(w, "profile.html", cliRes)
		return
	}

	if resp.StatusCode != 201 {
		cliRes.Message = "Error: " + string(data)
		cliRes.Status = resp.StatusCode
		err = Tpl.ExecuteTemplate(w, "editProfile.html", cliRes)
		return
	}

	cliRes.Message = "Updated successfully: " + string(data)
	cliRes.Status = resp.StatusCode
	Tpl.ExecuteTemplate(w, "profile.html", cliRes)

}
