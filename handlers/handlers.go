package handlers

import (
	"log"
	"net/http"

	"bitbucket.org/luisGsandoval/lu_sa-backend/config"
	"bitbucket.org/luisGsandoval/lu_sa-backend/routers"
	"github.com/gorilla/mux"
)

// HandleFuncs is the function that receives the requests
func HandleFuncs() {
	router := mux.NewRouter()

	// All routers can be found here
	// Public
	router.HandleFunc("/", routers.Login).Methods("GET")
	router.HandleFunc("/login", routers.LoginPost).Methods("POST")

	router.HandleFunc("/signup", routers.Singup).Methods("GET")
	router.HandleFunc("/signup", routers.SignupPost).Methods("POST")

	router.HandleFunc("/profile", routers.Profile).Methods("GET")
	router.HandleFunc("/editProfile", routers.EditprofileGet).Methods("GET")
	router.HandleFunc("/editProfile", routers.EditprofilePost).Methods("POST")

	router.HandleFunc("/not-found", routers.NotFound).Methods("get")
	router.NotFoundHandler = http.HandlerFunc(routers.NotFound)

	log.Printf("Server running on port: http://localhost:%s\n", config.Get("PORT"))
	log.Fatal(http.ListenAndServe(":"+config.Get("PORT"), router))

}
