package config

import (
	"os"
	"reflect"
)

// C stands for config where the env variables are going to b stored
type C struct {
	PORT      string `envName:"PORT" default:"8089"`
	BACKEND   string `envName:"BACKEND" default:"http://127.0.0.1:8088/api"`
	JWTSECRET string `envName:"JWTSECRET" default:"unbelieveable-how-this-thing-works"`
}

// Get environment variables or the default value
func Get(fld string) string {
	confg := reflect.TypeOf(C{})
	f, _ := confg.FieldByName(fld)
	v, _ := f.Tag.Lookup("envName")
	dv, _ := f.Tag.Lookup("default")
	envValue := os.Getenv(v)

	if envValue == "" {
		return dv
	}

	return envValue

}
