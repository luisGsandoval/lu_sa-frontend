module bitbucket.org/luisGsandoval/lu_sa-backend

go 1.14

require (
	github.com/dgrijalva/jwt-go v3.2.0+incompatible
	github.com/gorilla/mux v1.7.4
	go.mongodb.org/mongo-driver v1.3.5
)
